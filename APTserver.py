

from MotorTools.PyAPT import APTMotor
import Pyro4
import time



import numpy as np

@Pyro4.expose
class APTserver(object):
    
    def __init__(self):
        print ('server started')
        self.motors = []
        self.serial_numbers = []
        self.prec = 4
        
    def add_motor(self, serial_number):
        if not (serial_number in self.serial_numbers):
            self.serial_numbers += [serial_number]
            self.motors += [APTMotor(serial_number, HWTYPE=31)]
        
            print ('new motor connected')
        
            return len(self.motors) - 1
        
        else:
            return int(np.argmin(np.array(self.serial_numbers) - serial_number))
    
    def home(self,mid = 0):
        
        try:
            self.motors[mid].go_home()
            return 1
        except:
            return 0
        
    def mAbs(self,pos_mm ,mid = 0):
        pos_mm = np.round(pos_mm,self.prec)
        try:
            self.motors[mid].mAbs(pos_mm)
            
            while self.getPos(mid = mid) != pos_mm:
                time.sleep(0.01)
			
            return 1
        except:
            return 0
       
        
    def mRel(self,dis_mm , mid = 0):
        
        try:
            self.motors[mid].mRel(np.round(pos_mm,self.prec))
            return 1
        except:
            return 0
        
    def getPos(self,mid = 0):
        
        return np.round(self.motors[mid].getPos(),self.prec)
    
daemon = Pyro4.Daemon(host = "172.21.11.172", port = 9091)                # make a Pyro daemon
ns = Pyro4.locateNS()                  # find the name server
uri = daemon.register(APTserver)   # register the greeting maker as a Pyro object
ns.register("APTserver", uri)   # register the object with a name in the name server

print("Ready.")
daemon.requestLoop() 
            